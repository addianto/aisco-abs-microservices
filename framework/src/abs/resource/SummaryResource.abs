module MSummaryResource;
export SummaryWithSaldo, SummaryWithSaldoImpl;
import Summary, SummaryImpl from MSummaryModel;
import SummaryDb, SummaryDbImpl from MSummaryDbImpl;
import Income, IncomeImpl from MIncomeModel;
import IncomeDb, IncomeDbImpl from MIncomeDbImpl;
import Expense, ExpenseImpl from MExpenseModel;
import ExpenseDb, ExpenseDbImpl from MExpenseDbImpl;
import ABSHttpRequest from ABS.Framework.Http;
import Utility, UtilityImpl from ABS.Framework.Utility;
import ForeignUtility, ForeignUtilityImpl from ABS.Framework.ForeignUtility;

interface SummaryResource {
    List<Income> getIncomes(ABSHttpRequest request);
    List<Expense> getExpenses(ABSHttpRequest request);
    List<SummaryWithSaldo> list(ABSHttpRequest request);
    Summary detail(ABSHttpRequest request);
    List<Summary> create(ABSHttpRequest request);
    List<SummaryWithSaldo> save(ABSHttpRequest request);
    List<Summary> edit(ABSHttpRequest request);
    Summary update(ABSHttpRequest request);
    List<SummaryWithSaldo> delete(ABSHttpRequest request);
}

class SummaryResourceImpl implements SummaryResource {

    List<Income> getIncomes(ABSHttpRequest request) {
        IncomeDb orm = new local IncomeDbImpl();
        List<Income> incomes = orm.findAll("MIncomeModel.IncomeImpl_c");
        return incomes;
    }

    List<Expense> getExpenses(ABSHttpRequest request) {
        ExpenseDb expense_orm = new local ExpenseDbImpl();
        List<Expense> expenses = expense_orm.findAll("MExpenseModel.ExpenseImpl_c");
        return expenses;
    }

    List<SummaryWithSaldo> list(ABSHttpRequest request) {
        SummaryDb orm = new local SummaryDbImpl();
       // List<Summary> summaries = orm.findAll("MSummaryModel.SummaryImpl_c");
        List<SummaryWithSaldo> summariesWithSaldo = Nil;
        List<Income> allIncomes = this.getIncomes(request);
        List<Expense> allExpenses = this.getExpenses(request);

        Int count = 0;
        Int saldo = 0;

        while (count < length(allIncomes)) {
            SummaryWithSaldo summaryWithSaldo = new local SummaryWithSaldoImpl();
            Income thisLoopIncome = nth(allIncomes, count);

            Int incomeId = thisLoopIncome.getId();
            summaryWithSaldo.setId(incomeId);

            String incomeProgramNames = thisLoopIncome.getProgramName();
            summaryWithSaldo.setProgramName(incomeProgramNames);

            String incomeDatestamp = thisLoopIncome.getDatestamp();
            summaryWithSaldo.setDatestamp(incomeDatestamp);

            String incomeDescription = thisLoopIncome.getDescription();
            summaryWithSaldo.setDescription(incomeDescription);

            Int incomeIncome = thisLoopIncome.getAmount();
            summaryWithSaldo.setIncome(incomeIncome);

            summaryWithSaldo.setExpense(0);

            saldo = saldo + incomeIncome;
            summaryWithSaldo.setSaldo(saldo);
            List<SummaryWithSaldo> newSummaries = appendright(summariesWithSaldo, summaryWithSaldo);
            summariesWithSaldo = newSummaries;
            count = count + 1;
        }

        count = 0;
        while (count < length(allExpenses)) {
            SummaryWithSaldo summaryWithSaldo = new local SummaryWithSaldoImpl();
            Expense thisLoopExpense = nth(allExpenses, count);

            Int expenseId = thisLoopExpense.getId();
            summaryWithSaldo.setId(expenseId);

            String expenseProgramNames = thisLoopExpense.getProgramName();
            summaryWithSaldo.setProgramName(expenseProgramNames);

            String expenseDatestamp = thisLoopExpense.getDatestamp();
            summaryWithSaldo.setDatestamp(expenseDatestamp);

            String expenseDescription = thisLoopExpense.getDescription();
            summaryWithSaldo.setDescription(expenseDescription);

            summaryWithSaldo.setIncome(0);

            Int expenseValue = thisLoopExpense.getAmount();
            summaryWithSaldo.setExpense(expenseValue);

            saldo = saldo - expenseValue;
            summaryWithSaldo.setSaldo(saldo);
            summariesWithSaldo = appendright(summariesWithSaldo, summaryWithSaldo);
            count = count + 1;
        }

        // sorting
        ForeignUtility foreignUtility = new local ForeignUtilityImpl();
        List<SummaryWithSaldo> sortedSummaries = foreignUtility.sort(summariesWithSaldo, "saldo"); // param 2 doesn't have effect yet
        return sortedSummaries;
    }

    Summary detail(ABSHttpRequest request) {
        String id = request.getInput("id");
        String condition = "id=" + id;
        SummaryDb orm = new local SummaryDbImpl();
        return orm.findByAttributes("MSummaryModel.SummaryImpl_c",condition);
    }

    List<Summary> create(ABSHttpRequest request) {
        return Nil;
    }

    List<SummaryWithSaldo> save(ABSHttpRequest request) {
        Utility utility = new local UtilityImpl();

        SummaryDb orm = new local SummaryDbImpl();
        Summary summary = new local SummaryImpl();

        String datestamp = request.getInput("datestamp");
        String description = request.getInput("description");
        String incomeStr = request.getInput("income");
        Int income = utility.stringToInteger(incomeStr);
        String expenseStr = request.getInput("expense");
        Int expense = utility.stringToInteger(expenseStr);
        String programName = request.getInput("programName");


        summary.setDatestamp(datestamp);
        summary.setDescription(description);
        summary.setIncome(income);
        summary.setExpense(expense);
        summary.setProgramName(programName);
        orm.save(summary);
        return this.list(request);
    }

    List<Summary> edit(ABSHttpRequest request) {
        String id = request.getInput("id");
        String condition = "id=" + id;
        SummaryDb orm = new local SummaryDbImpl();
        Summary summary = orm.findByAttributes("MSummaryModel.SummaryImpl_c", condition);

        List<Summary> dataModel = Nil;
        return appendright(dataModel, summary);
    }

    Summary update(ABSHttpRequest request) {
        Utility utility = new local UtilityImpl();

        String id = request.getInput("id");
        String condition = "id=" + id;
        SummaryDb orm = new local SummaryDbImpl();
        Summary summary = orm.findByAttributes("MSummaryModel.SummaryImpl_c",condition);

        String datestamp = request.getInput("datestamp");
        String description = request.getInput("description");
        String incomeStr = request.getInput("income");
        Int income = utility.stringToInteger(incomeStr);
        String expenseStr = request.getInput("expense");
        Int expense = utility.stringToInteger(expenseStr);
        String programName = request.getInput("programName");

        summary.setDatestamp(datestamp);
        summary.setDescription(description);
        summary.setIncome(income);
        summary.setExpense(expense);
        summary.setProgramName(programName);
        orm.save(summary);

        return this.detail(request);
    }

    List<SummaryWithSaldo> delete(ABSHttpRequest request) {
        String id = request.getInput("id");
        String condition = "id=" + id;
        SummaryDb orm = new local SummaryDbImpl();
        Summary summary = orm.findByAttributes("MSummaryModel.SummaryImpl_c", condition);

        orm.delete(summary);
        return this.list(request);
    }
}

interface SummaryWithSaldo extends Summary {
    Int getSaldo();
    Unit setSaldo(Int saldo);
}

class SummaryWithSaldoImpl implements SummaryWithSaldo {
    [PK] Int id = 0;
    String datestamp = "";
    String description = "";
    Int income = 0;
    Int expense = 0;
    String programName = "";
    Int saldo = 0;

    Int getId() { return this.id; }
    Unit setId(Int id) { this.id = id; }
    String getProgramName() { return this.programName; }
    Unit setProgramName(String programName) { this.programName = programName; }
    String getDatestamp() { return this.datestamp; }
    Unit setDatestamp(String datestamp) { this.datestamp = datestamp; }
    String getDescription() { return this.description; }
    Unit setDescription(String description) { this.description = description; }
    Int getIncome() { return this.income; }
    Unit setIncome(Int income) { this.income = income; }
    Int getExpense() { return this.expense; }
    Unit setExpense(Int expense) { this.expense = expense; }
    Int getSaldo() { return this.saldo; }
    Unit setSaldo(Int saldo) { this.saldo = saldo + this.income - this.expense; }
}